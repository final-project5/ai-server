import datetime
import sqlite3
import time
import requests
import json


domain_serving_image = 'http://192.168.1.3:8080/'
domain_SMRT = 'http://192.168.1.3:9999/'


def send_post_process(id_camera, weight, image_folder):
    time_now = datetime.datetime.now().strftime('%d-%m-%Y %H:%M')
    link = domain_SMRT + 'api/update_cameras'
    link_image = domain_serving_image + 'static/' + image_folder
    print('image fordel: ', image_folder)
    r = requests.post(link,
                      json={
                          "id": id_camera,
                          "weight": weight,
                          "time": str(time_now),
                          "image": link_image,
                          "can_go": True
                      }
                      )
    print({"id": id_camera,
                          "weight": weight,
                          "time": str(time_now),
                          "image": link_image,
                          "can_go": True}
          )
    #print(r.status_code, r.reason)


conn = sqlite3.connect('last_data_AI.sqlite')


while True:
    cursor = conn.execute("SELECT ID, ID_CAMERA, WEIGHT, IMAGE_LINK from AIDATA")
    index = 1
    for row in cursor:
        send_post_process(str(row[1]), str(row[2]), str(row[3]))
        index += 1
        if index == 21:
            time.sleep(30)
            index = 0


