from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    text = 'test'
    return text

if __name__ == '__main__':
   app.run(port=8080,host='0.0.0.0')