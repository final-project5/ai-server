import numpy as np
import cv2

cap = cv2.VideoCapture(r"C:\Users\phucd\Documents\final project\PhucDuong\AI server\process_video\videos\11%.mp4")
index = 0
folder = "2/"
index_image = 1
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if index % 24 == 0:
        # Display the resulting frame
        # cv2.imshow('frame', frame)
        file_name = 'frames_cutted/' + folder + str(index_image) + ".jpg"
        index_image += 1
        cv2.imwrite(file_name, frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    index += 1
    if index_image == 31:
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()