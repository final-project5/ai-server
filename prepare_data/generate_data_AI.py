import sqlite3
import datetime
from mrcnn.config import Config
from mrcnn import model as modellib
import colorsys
import imutils
import random
import cv2
import os

CLASS_NAMES = open('../coco_labels.txt').read().strip().split("\n")
hsv = [(i / len(CLASS_NAMES), 1, 1.0) for i in range(len(CLASS_NAMES))]
COLORS = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
random.seed(42)
random.shuffle(COLORS)



#connect database
connAI = sqlite3.connect('AI_DATA.sqlite')
connAI.execute('''CREATE TABLE AIDATA
         (ID INTEGER PRIMARY KEY AUTOINCREMENT,
         ID_CAMERA CHAR(50),
         WEIGHT        CHAR(50),
         TIME            CHAR(50),
         CAN_GO        CHAR(10),
         IMAGE_LINK  CHAR(100));''')

connAI.commit()


connCamera = sqlite3.connect('../camera.sqlite')

class SimpleConfig(Config):
    NAME = "coco_inference"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = len(CLASS_NAMES)


config = SimpleConfig()


# initialize the Mask R-CNN model for inference and then load the
# weights
print("[INFO] loading Mask R-CNN model...")
model = modellib.MaskRCNN(mode="inference", config=config,
	model_dir=os.getcwd())
model.load_weights('../mask_rcnn_coco.h5', by_name=True)


def send_data_to_sqlite(id_camera,N,image_path):
    global connCamera, connAI
    area = None
    cursor = connCamera.execute("SELECT area from Cameras")
    for row in cursor:
        area = row[0]
        print("area: ",area)

    #sau nay co domain se doi image_link=domain+image_path
    image_link = image_path.split('static/')[1]
    # calculate % road
    weight_road = int(float(N) / float(area) * 100)
    timenow = datetime.datetime.now().strftime('%d-%m-%Y %H:%M')

    connAI.execute("INSERT INTO AIDATA (ID_CAMERA,WEIGHT,TIME,CAN_GO,IMAGE_LINK) \
               VALUES ('"+id_camera+"','"+str(weight_road)+"','"+str(timenow)+"','True','"+image_link+"')")
    connAI.commit()


def predict_for_1_image(id_camera, image_path):
    print("image path: ",image_path)
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = imutils.resize(image, width=512)

    print("[INFO] making predictions with Mask R-CNN...")
    r = model.detect([image], verbose=1)[0]

    num = 1
    N = 0
    person = 0
    car = 0
    motorcycle = 0
    bicycle = 0
    bus = 0
    truck = 0

    for i in range(0, len(r["scores"])):
        classID = r["class_ids"][i]
        label = CLASS_NAMES[classID]

        print("num" + str(num) + " " + "label: " + label)
        num += 1
        if label == "person":
            N += 4
            person += 1
        if label == "motorcycle":
            N += 4
            motorcycle += 1
        if label == "bicycle":
            N += 4
            bicycle += 1
        if label == "car":
            N += 12
            car += 1
        if label == "bus":
            N += 25
            bus += 1
        if label == "truck":
            N += 25
            truck += 1

    print('N = ', N)
    print('person: ', person)
    print('motorcycle: ', motorcycle)
    print('bicycle: ', bicycle)
    print('car: ', car)
    print('bus: ', bus)
    print('truck: ', truck)

    send_data_to_sqlite(id_camera, N, image_path)
    print('post to database successfully')


#main

#get camera id form camra.sqlite


cursor = connCamera.execute("SELECT ID from Cameras")

id_cameras = {}
index_camera = 1
for row in cursor:
    id_cameras[str(index_camera)] = row[0]
    index_camera += 1
    print("ID = ", row[0])


#30 images / folder
for i in range(0, 30):
    #("index: ",j)
    #("id camera: ",id_cameras[j])
    for j in id_cameras:
        if int(j) < 7:
            image_path = r'../API/static/'+str(j)+'/'+str(i)+".jpg"
            # print('image_path: ',image_path)
            predict_for_1_image(id_cameras[j], image_path)
        else:
            break

connCamera.close()
connAI.close()









